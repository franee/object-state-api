module Api
  module V1
    class ObjectStatesController < ApplicationController
      def index
        osq = ObjectStateQueryService.new(params)
        osq.do_query

        render json: osq.result
      end

      def create
        csv_consumer = CSVConsumerService.new(params)
        csv_consumer.consume

        render json: csv_consumer.result
      end
    end
  end
end
