class ObjectStateQueryService
  def initialize(params)
    @error_messages = []

    validate_params(params)

    @obj_id    = params[:obj_id]
    @obj_type  = params[:obj_type]
    @timestamp = params[:ts]

    @object = nil
  end

  def do_query
    return unless @error_messages.empty?

    @object = ObjectState.where(obj_id:    @obj_id,
                                obj_type:  @obj_type,
                                timestamp: @timestamp).first
  end

  def result
    if @error_messages.empty?
      @object.try(:properties) || {}
    else
      { errors: @error_messages.join(', ') }
    end
  end

  private

  def validate_params(params)
    %i[obj_id obj_type ts].each do |field|
      if params[field].blank?
        @error_messages << format(':%s is missing from query string', field)
      end
    end

    return if params[:ts].blank?

    if params[:ts].to_s =~ /^\d+$/
      if params[:ts].to_i > ObjectState::MAX_TIMESTAMP
        @error_messages << format('Invalid Timestamp %s > %s', params[:ts], ObjectState::MAX_TIMESTAMP)
      end
    else
      @error_messages << format('Invalid Timestamp => %s', params[:ts])
    end
  end
end
