require 'csv'

class CSVConsumerService
  def initialize(params)
    @csv = params[:csv]

    validate_csv!

    @error_messages = []
    @counter = 0
    @csv_headers = []
    @current_line_no = 0
  end

  def consume
    return unless @error_messages.empty?

    extract_row do |row|
      store(row)
    end
  end

  def result
    result_h = { inserted: @counter }

    return result_h if @error_messages.empty?

    result_h[:errors] = @error_messages.join(', ')

    result_h
  end

  private

  def validate_csv!
    return if @csv.present? && @csv.respond_to?(:tempfile)

    raise CSVError, 'Missing or Invalid CSV file'
  end

  # Need to manually open the file so we can handle unescaped json string
  def extract_row
    File.foreach(@csv.tempfile) do |csv_line|
      @current_line_no += 1

      extract_and_hashify(csv_line) do |row|
        yield(row)
      end
    end
  end

  def extract_and_hashify(csv_line)
    row = parse_csv_line(csv_line)

    if @csv_headers.empty?
      @csv_headers = extract_headers(row)
    else
      yield Hash[@csv_headers.zip(row)]
    end
  end

  # clean unescaped json
  def parse_csv_line(csv_line)
    CSV.parse(csv_line.gsub('\"', '""')).first
  end

  HEADERS = %i[object_id object_type timestamp object_changes].freeze

  def extract_headers(row)
    headers = row.map(&:to_sym)

    fields = headers - HEADERS
    unless fields.empty?
      raise CSVError, format('Invalid fields in CSV file => %s', fields.join(','))
    end

    headers
  end

  def store(row)
    return if exists?(row)

    os = ObjectState.new(obj_id:     row[:object_id],
                         obj_type:   row[:object_type],
                         timestamp:  row[:timestamp],
                         properties: JSON.parse(row[:object_changes]))

    if os.save
      @counter += 1
    else
      @error_messages << format('line[%s]. %s', @current_line_no, os.errors.full_messages.join(', '))
    end
  rescue JSON::ParserError => e
    @error_messages << format('line[%s]. %s', @current_line_no, e.message)
  end

  def exists?(row)
    ObjectState.exists?(obj_id:     row[:object_id],
                        obj_type:   row[:object_type],
                        timestamp:  row[:timestamp])
  end
end
