class ObjectState < ApplicationRecord
  # https://en.wikipedia.org/wiki/Year_2038_problem
  MAX_TIMESTAMP = 2_147_483_647

  validates :obj_id, :obj_type, :timestamp, :properties, presence: true
  validates :timestamp, numericality: { only_integer: true,
                                        less_than_or_equal_to: MAX_TIMESTAMP }

  serialize :properties, JSON
end
