require 'rails_helper'

RSpec.describe '/api/v1/object_states api', type: :request do
  include_examples 'common for /api/object_states & /api/v1/object_states', '/api/v1'
  include_examples 'common errors /api/object_states & /api/v1/object_states', '/api/v1'
end
