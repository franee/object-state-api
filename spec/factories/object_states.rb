STATUS = %w[paid unpaid].freeze
TYPES = %w[Product Invoice Order].freeze

def status
  STATUS.sample
end

props_generator = proc do
  [{ status: status },
   { customer_name: Faker::Name.first_name,
     customer_address: Faker::Address.street_address, status: status },
   { name: Faker::Food.ingredient, price: Faker::Number.between(50, 200),
     stock_levels: Faker::Number.between(100, 2500) },
   { order_id: Faker::Number.between(1, 100), product_ids: [1, 3, 5, 6],
     status: status, total: Faker::Number.between(500, 1000) },
   { status: status, ship_date: Faker::Date.between(1.year.ago, Time.zone.today).to_s }].sample
end

FactoryGirl.define do
  factory :object_state do
    obj_id     { Faker::Number.between(1, 100) }
    obj_type   { TYPES.sample }
    timestamp  { Faker::Date.between(1.year.ago, Time.zone.today).to_time.to_i }
    properties { props_generator.call }
  end
end
