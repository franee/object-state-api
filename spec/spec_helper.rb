RSpec.configure do |config|
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.shared_context_metadata_behavior = :apply_to_host_groups

  config.default_formatter = 'doc'

  config.order = :random

  Kernel.srand config.seed
end

# custom matchers

RSpec::Matchers.define :be_jsonapi_error_response do |http_status|
  match do |actual|
    parsed_actual = JSON.parse(actual)

    errors = parsed_actual['errors']

    return false if errors.blank? || errors.first.blank?

    h = errors.first

    h['status'] == Rack::Utils::SYMBOL_TO_STATUS_CODE[http_status] &&
      h['source']['pointer'] == "#{request.method} #{request.path}"
  end
end
