RSpec.shared_examples 'should respond with an error json' do |http_status|
  it 'should respond with an json error message' do
    do_request

    expect(response.body).to be_jsonapi_error_response(http_status)
  end
end

RSpec.shared_examples 'common errors /api/object_states & /api/v1/object_states' do |prefix|
  context 'server error' do
    let(:file) do
      fixture_file_upload('/files/double_quotes_data.csv', 'text/csv')
    end

    let(:do_request) do
      post "#{prefix}/object_states", params: { csv: file }
    end

    before(:each) do
      allow(CSVConsumerService).to receive(:new) { raise 'server error' }
    end

    include_examples 'should respond with an error json', :internal_server_error
  end

  context 'routing errors' do
    context 'non-existent routes' do
      describe "GET #{prefix}/does_not_exist" do
        let(:do_request) do
          get "#{prefix}/does_not_exist"
        end

        include_examples 'should respond with an error json', :bad_request
      end

      describe "POST #{prefix}/does_not_exist" do
        let(:do_request) do
          post "#{prefix}/does_not_exist"
        end

        include_examples 'should respond with an error json', :bad_request
      end
    end
  end
end

RSpec.shared_examples 'common for /api/object_states & /api/v1/object_states' do |prefix|
  describe "POST #{prefix}/object_states" do
    context 'invalid csv' do
      context 'no csv file given' do
        let(:do_request) do
          post "#{prefix}/object_states"
        end

        include_examples 'should respond with an error json', :bad_request
      end

      context 'wrong csv format' do
        let(:file) do
          fixture_file_upload('/files/wrong_format_data.csv', 'text/csv')
        end

        let(:do_request) do
          post "#{prefix}/object_states", params: { csv: file }
        end

        include_examples 'should respond with an error json', :bad_request
      end

      context 'malformed csv' do
        let(:file) do
          fixture_file_upload('/files/malformed_data.csv', 'text/csv')
        end

        let(:do_request) do
          post "#{prefix}/object_states", params: { csv: file }
        end

        include_examples 'should respond with an error json', :bad_request
      end
    end

    context 'valid csv' do
      context 'csv with escaped quotes \"' do
        let(:file) do
          fixture_file_upload('/files/escaped_quotes_data.csv', 'text/csv')
        end

        let(:do_request) do
          post "#{prefix}/object_states", params: { csv: file }
        end

        it 'should return success' do
          do_request

          expect(response).to be_success
          expect(response).to have_http_status(:ok)
        end
      end

      context 'csv with escaped quotes ""' do
        let(:file) do
          fixture_file_upload('/files/double_quotes_data.csv', 'text/csv')
        end

        let(:do_request) do
          post "#{prefix}/object_states", params: { csv: file }
        end

        it 'should return success' do
          do_request

          expect(response).to be_success
          expect(response).to have_http_status(:ok)
        end
      end
    end
  end
end
