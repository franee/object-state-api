require 'rails_helper'

describe ObjectStateQueryService do
  let(:osq_factory) do
    proc do |h|
      ObjectStateQueryService.new(h)
    end
  end

  let(:osq) do
    osq_factory.call(params)
  end

  describe '#initialize' do
    context 'valid params' do
      let(:params) do
        { obj_id: 1, obj_type: 'Test', ts: 1 }
      end

      it 'should have no errors' do
        errors = osq.instance_variable_get(:@error_messages)
        expect(errors).to be_empty
      end
    end

    context 'invalid params' do
      context 'missing params' do
        let(:params) do
          {}
        end

        it 'should have errors' do
          errors = osq.instance_variable_get(:@error_messages)
          expect(errors).not_to be_empty
        end
      end

      context 'timestamp issues' do
        context 'timestamp > 2038' do
          let(:ts) do
            ObjectState::MAX_TIMESTAMP
          end

          let(:params) do
            { obj_id: 1, obj_type: 'Test', ts: (ts + 1) }
          end

          it 'should have an invalid timestamp' do
            errors = osq.instance_variable_get(:@error_messages)
            expect(errors.size).to eq(1)
            expect(errors).to include(format('Invalid Timestamp %s > %s', (ts + 1), ts))
          end
        end

        context 'invalid timestamp format' do
          let(:ts) do
            '123456a'
          end

          let(:params) do
            { obj_id: 1, obj_type: 'Test', ts: ts }
          end

          it 'should have an invalid timestamp' do
            errors = osq.instance_variable_get(:@error_messages)
            expect(errors.size).to eq(1)

            expected = format('Invalid Timestamp => %s', ts)
            expect(errors).to include(expected)
          end
        end
      end
    end
  end

  describe '#do_query' do
    context 'no errors' do
      let(:params) do
        { obj_id: 1, obj_type: 'Test', ts: 1 }
      end

      it 'should query the db' do
        expect(ObjectState).to receive(:where).and_return([])

        osq.do_query
      end
    end

    context 'with errors' do
      let(:params) do
        {}
      end

      it 'should not query the db' do
        expect(ObjectState).not_to receive(:where)

        osq.do_query
      end
    end
  end

  describe '#result' do
    context 'no errors' do
      let(:params) do
        { obj_id: 1, obj_type: 'Test', ts: 1 }
      end

      context 'nil object_state' do
        it 'should return an empty hash' do
          expect(osq.result).to eq({})
        end
      end

      context 'with object_state' do
        let!(:os) do
          create(:object_state)
        end

        let(:props) do
          os.properties
        end

        let(:params) do
          { obj_id: os.obj_id, obj_type: os.obj_type, ts: os.timestamp }
        end

        it 'should return the properties hash' do
          osq.do_query
          expect(osq.result).to eq(props)
        end
      end
    end

    context 'with errors' do
      let(:params) do
        {}
      end

      it 'should return the errors hash' do
        osq.do_query
        expect(osq.result).to include(:errors)
      end
    end
  end
end
