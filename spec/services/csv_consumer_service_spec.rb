require 'rails_helper'

RSpec.shared_examples 'csv_consumer does not raise error' do
  it 'should not raise an error' do
    expect {
      csv
    }.not_to raise_error
  end
end

RSpec.shared_examples 'csv_consumer raises error' do |error_class|
  it 'should raise an error' do
    expect {
      csv
    }.to raise_error(error_class)
  end
end

RSpec.shared_examples 'csv_consumer consume' do
  it 'should insert to database' do
    expect {
      csv.consume
    }.to change {
      ObjectState.count
    }
  end
end

RSpec.shared_examples 'csv_consumer consume error' do |error_class|
  it 'should raise an error' do
    expect {
      csv.consume
    }.to raise_error(error_class)
  end
end

describe CSVConsumerService do
  let(:csv_factory) do
    proc do |h|
      CSVConsumerService.new(h)
    end
  end

  let(:csv) do
    csv_factory.call(params)
  end

  let(:valid_file_double_quotes) do
    fixture_file_upload('/files/double_quotes_data.csv', 'text/csv')
  end

  let(:valid_file_escaped_quotes) do
    fixture_file_upload('/files/escaped_quotes_data.csv', 'text/csv')
  end

  let(:malformed_file) do
    fixture_file_upload('/files/malformed_data.csv', 'text/csv')
  end

  let(:wrong_format_file) do
    fixture_file_upload('/files/wrong_format_data.csv', 'text/csv')
  end

  describe '#initialize' do
    context 'valid params' do
      context 'csv is present and responds to tempfile' do
        let(:params) do
          { csv: instance_double('Hello', tempfile: true) }
        end

        include_examples 'csv_consumer does not raise error'
      end
    end

    context 'invalid params' do
      context 'csv not in params' do
        let(:params) do
          {}
        end

        include_examples 'csv_consumer raises error', CSVError
      end

      context 'csv file does not respond to tempfile' do
        let(:params) do
          { csv: instance_double('File') }
        end

        include_examples 'csv_consumer raises error', CSVError
      end
    end
  end

  describe '#consume' do
    context 'no errors' do
      context 'double quoted json' do
        let(:params) do
          { csv: valid_file_double_quotes }
        end

        include_examples 'csv_consumer does not raise error'
        include_examples 'csv_consumer consume'
      end

      context 'single quoted json' do
        let(:params) do
          { csv: valid_file_escaped_quotes }
        end

        include_examples 'csv_consumer does not raise error'
        include_examples 'csv_consumer consume'
      end
    end

    context 'with errors' do
      context 'exceptions raised' do
        context 'malformed' do
          let(:params) do
            { csv: malformed_file }
          end

          include_examples 'csv_consumer consume error', CSV::MalformedCSVError
        end

        context 'invalid format' do
          let(:params) do
            { csv: wrong_format_file }
          end

          include_examples 'csv_consumer consume error', CSVError
        end
      end

      context 'error_messages' do
        context 'JSON parser error' do
          let(:params) do
            { csv: valid_file_escaped_quotes }
          end

          before(:each) do
            allow(JSON).to receive(:parse).and_raise(JSON::ParserError, 'boom')
          end

          it 'should have an error message' do
            csv.consume
            errors = csv.instance_variable_get(:@error_messages)
            expect(errors).to_not be_empty
          end

          include_examples 'csv_consumer does not raise error'
        end
      end
    end
  end

  describe '#result' do
    context 'no errors' do
      let(:params) do
        { csv: valid_file_escaped_quotes }
      end

      it 'should a hash with inserted counts' do
        csv.consume
        expect(csv.result).to include(:inserted)
        expect(csv.result).not_to include(:errors)
      end
    end

    context 'with errors' do
      let(:params) do
        { csv: valid_file_escaped_quotes }
      end

      before(:each) do
        allow(JSON).to receive(:parse).and_raise(JSON::ParserError, 'boom')
      end

      it 'should return the errors hash' do
        csv.consume
        expect(csv.result).to include(:errors)
        expect(csv.result).to include(inserted: 0)
      end
    end
  end
end
