# Objectives
Design and develop API endpoints for object states.

# Requirements
Use ruby 2.4.1 & rails 5.1.2

# API endpoints
* POST /api/v1/object_states/
  * Accepts a CSV file with the ff columns:
    * object_id - Integer
    * object_type - String
    * timestamp - Unix timestamp
    * object_changes - Serialized json data of object properties

* GET /api/v1/object_states?id=1&type=Product&ts=1484733173
  * Query state of object at specific timestamp
    * id - object_id
    * type - object_type
    * ts - timestamp

# DEMO
* Upload CSV
  * Download test data from [here](https://object-state-api.herokuapp.com/test_data.csv)
  * In shell terminal:
    ```
    $ curl -X POST -F 'csv=@test_data.csv' https://object-state-api.herokuapp.com/api/v1/object_states
    ```
* Querying Object States
  * In the browser: [https://object-state-api.herokuapp.com/api/object_states?obj_id=33&obj_type=Order&ts=1475596800](https://object-state-api.herokuapp.com/api/object_states?obj_id=33&obj_type=Order&ts=1475596800)
