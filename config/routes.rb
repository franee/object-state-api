Rails.application.routes.draw do
  # namespaced routes for api versioning
  namespace :api, defaults: { format: 'json' } do
    # standard apis that just points to the latest version
    resources :object_states, controller: 'v1/object_states', only: %i[index create]

    # v1 api
    namespace :v1 do
      resources :object_states, only: %i[index create]
    end

    # add v2 here later
  end

  # catch everything else and give a routing error message
  match '*a', to: 'api/errors#routing', via: :all

  get '/', to: redirect('api/object_states')
end
