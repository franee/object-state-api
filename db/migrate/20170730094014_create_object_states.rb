class CreateObjectStates < ActiveRecord::Migration[5.1]
  def change
    create_table :object_states do |t|
      t.integer :obj_id
      t.string  :obj_type
      t.integer :timestamp
      t.text    :properties

      t.timestamps
    end

    add_index(:object_states, %i[obj_id obj_type timestamp])
  end
end
